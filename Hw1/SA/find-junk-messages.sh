# /usr/bin/env bash 

grep -v -P '\(*\)' | while read line
do
	if [ `echo $line | wc -w` -eq 7 ]; then
		if [ `echo $line | tr -s ' ' | cut -d' ' -f7` == $1 ]; then
			echo $line | cut -d' ' -f1 | tr -d '[:punct:]'
		fi
	fi
done
