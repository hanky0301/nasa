Homework 2 - Unix Basic Commands, Concepts and File Permission
==============================================================


Shell Scripts
-------------
A big advantage of using commands is that it is easy to combine several simple
commands to do a more complex task. Writing shell scripts is the most common
and easy way to make use of command line tools to automate routine tasks,
manipulate files or process output.

Please write a shell script for each following problem and send all scripts
in a tarball. All scripts should have their execute permission set, so it can
be used as a regular executable.

You are allowed to use the standard (POSIX) shell or GNU Bash.

 - If you use the standard shell, the first line of your script must be
   `#!/bin/sh`. You can use dash on GNU/Linux or sh on *BSD to test your script.
   You are encouraged to write scripts using the standard shell, but you must
   not use any bash-specific features in a `#!/bin/sh` script. You can use
   `checkbashisms` command to check whether the syntax is allowed by the
   standard.
 - If you use GNU Bash, the first line of your script must be
   `#!/usr/bin/env bash`. Please don't use `#!/bin/bash` because hard-coding
   the path of bash is known to cause problems on many platforms, including
   most *BSD systems.


Find queue IDs of junk messages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
You are notified that some workstation accounts are stolen and being used to
send junk mail. After you lock all stolen accounts and drop packets from
attackers, you should delete all junk messages submitted by attackers in the
queue of the mail server. You are asked to write a script to read the output of
`mailq` command and print queue IDs of junk messages, so we can remove them
from queue using `postsuper -d` command.

When the user of your script run the following command:

------------------------------------------------------------------------------
mailq | ./find-junk-messages.sh b12345678@csie.ntu.edu.tw | postsuper -d -
------------------------------------------------------------------------------

Your script should print the IDs of all messages submitted by
`b12345678@csie.ntu.edu.tw` account.

 - Input: the format output by `mailq` command
------------------------------------------------------------------------------
-Queue ID- --Size-- ----Arrival Time---- -Sender/Recipient-------
751068E490     8723 Tue Jan 27 00:53:38  owner-b12@list.csie.ntu.edu.tw
(temporary failure. Command output: local: fatal: execvp /usr/ucb/vacation: No such file or directory)
                                         b12345678@csie.ntu.edu.tw

DB66D8E20E     3098 Wed Jan 28 02:37:58  b12345678@csie.ntu.edu.tw
(host mx2.comcast.net[68.87.20.5] refused to talk to me: 554 resimta-ch2-06v.sys.comcast.net comcast 140.112.30.68 Comcast block for spam.  Please see http://postmaster.comcast.net/smtp-error-codes.php#BL000000)
                                         ajolsson@comcast.net
                                         pmidtvedt@comcast.net

772018E234*    2898 Wed Jan 28 07:33:20  b12345678@csie.ntu.edu.tw
(delivery temporarily suspended: host mx2.comcast.net[68.87.20.5] refused to talk to me: 554 resimta-ch2-07v.sys.comcast.net comcast 140.112.30.68 Comcast block for spam.  Please see http://postmaster.comcast.net/smtp-error-codes.php#BL000000)
                                         world@comcast.net

BF608910F4     9095 Thu Jan 29 12:24:32  b23456789@csie.ntu.edu.tw
        (connect to abc.csie.ntu.edu.tw[140.112.123.234]:25: Connection refused)
                                         hello@abc.csie.ntu.edu.tw

-- 24 Kbytes in 4 Requests.
------------------------------------------------------------------------------

 - Output: the format accepted by `postsuper -d` command
------------------------------------------------------------------------------
DB66D8E20E
772018E234
------------------------------------------------------------------------------


A compiler wrapper to workaround some broken build systems
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
You are going to compile a large project which has hundreds of Makefiles.
Unfortunately, its build system is much broken, and it hard-code several
compiling and linking arguments which are not easy to change using
environment variables. Therefore, you decide to write a compiler wrapper to
trick its build system to run your wrapper instead of running the compiler
directly, so you can add needed arguments.

Please write a wrapper which executes the real program located in `/usr/bin`
with some arguments added. Your script should exit with the same status as
the real program, so the build system can know whether the compilation is
successful. You should add compiling argument `-isystem /usr/local/include`
before all the other arguments or linking argument `-Wl,-Y/usr/local/lib`
after all the other arguments depending on the following rule:

 - If your script is named after `cc` or `c++`, you should add both the
   compiling argument and the linking argument. However, if any one of `-c`,
   `-S` or `-E` is used, you should add only the compiling argument.
 - If your script is named after `cpp`, you should add only the compiling
   argument.

You can write only one script. We will make hard links or symbolic links to
your script, so it can have several names. Please see the following examples.

------------------------------------------------------------------------------
# When an user run
cc world.c -c -o world.o
# Your script should run
/usr/bin/cc -isystem /usr/local/include world.c -c -o world.o

# When an user run
c++ -o project project.o hello.o -lcrypto -lm
# Your script should run
/usr/bin/c++ -isystem /usr/local/include -o project project.o hello.o -lcrypto \
  -lm -Wl,-Y/usr/local/lib

# When an user run
cpp -D_POSIX_C_SOURCE=200809L -D_XOPEN_SOURCE=700 mypkg.c
# Your script should run
/usr/bin/cpp -isystem /usr/local/include -D_POSIX_C_SOURCE=200809L \
  -D_XOPEN_SOURCE=700 mypkg.c
------------------------------------------------------------------------------


Print the most recent login time and desktop session of an user
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Many desktop and workstation systems have
http://www.freedesktop.org/wiki/Software/AccountsService/[AccountsService]
installed now. Although it provides a simple API for applications to use,
there is no official command line tool to read information from the service.
Please write a script to show the most recent login time in
YYYY-MM-DD hh:mm:ss format and the most recently used desktop session
of a user specified on the command line.

You don't need to install AccountsService or learn the API of AccountsService
to complete this task. You should be able to generate similar output to test
your script.

For example, when an user run

------------------------------------------------------------------------------
./show-recent-logintime-and-desktopsession ta217
------------------------------------------------------------------------------

you can use following commands to retrieve needed information.

------------------------------------------------------------------------------
# Find the passwd entry of the user
$ getent passwd ta217
ta217:x:41610:200:ta217:/home/dept/ta/ta217:/bin/bash
------------------------------------------------------------------------------

------------------------------------------------------------------------------
# As we only need the uid of the user, using id command is enough
$ id ta217
uid=41610(ta217) gid=200(ta) groups=200(ta),1000(admin)
------------------------------------------------------------------------------

------------------------------------------------------------------------------
# Get the most recent login time of the user from AccountsService
$ dbus-send --system --print-reply --dest=org.freedesktop.Accounts \
> --type=method_call /org/freedesktop/Accounts/User41610 \
> org.freedesktop.DBus.Properties.Get \
> string:org.freedesktop.Accounts.User string:LoginTime
method return sender=:1.18 -> dest=:1.165 reply_serial=2
   variant       int64 1422614246
------------------------------------------------------------------------------

------------------------------------------------------------------------------
# Get the most recently used desktop session of the user from AccountsService
$ dbus-send --system --print-reply --dest=org.freedesktop.Accounts \
> --type=method_call /org/freedesktop/Accounts/User41610 \
> org.freedesktop.DBus.Properties.Get \
> string:org.freedesktop.Accounts.User string:XSession
method return sender=:1.18 -> dest=:1.166 reply_serial=2
   variant       string "gnome-classic"
------------------------------------------------------------------------------

Your script should print

------------------------------------------------------------------------------
ta217 gnome-classic 2015-01-30 18:37:26
------------------------------------------------------------------------------


Shared Folders and Files
------------------------
It is common to have a shared folder or shared git repository when working on
a project with several people in a team. ACL can manage complex settings that
the standard Unix permission is not be able to. You can use `getfacl` and
`setfacl` to view and manage ACL settings.

Please create a shared folder with its permission set to satisfy the following
requirements:

 . There are 3 team members: `student1`, `student2` and `student3`. All of
   them are members of `student` group. They can add, modify or delete any file
   and folder in this shared folder, regardless the owner of the file or folder.
 . There are 2 TAs: `ta1` and `ta2`.
   They are allowed to read all files and folders in this shared folder, but
   addtion, modification and deletion is not allowed.
 . There is 1 professor: `prof1`, having the same permission as TAs.
 . All other people, including users in `student` group that are not a team
   member, are not permitted to access any file in this shared folder.

The following message is a sample output of `getfacl` command running on the
shared folder with several permission sets replaced by underscores. Please
fill in all underscores with correct permission settings.

------------------------------------------------------------------------------
     1  # file: project_shared_folder
     2  # owner: student1
     3  # group: student
     4  user::rwx
     5  user:student1:rwx
     6  user:student2:___
     7  user:student3:___
     8  user:ta1:___
     9  user:ta2:___
    10  user:prof1:r-x
    11  group::___
    12  mask::___
    13  other::---
------------------------------------------------------------------------------

[NOTE]
Although ACL is an abandoned POSIX standard, it is already implemented on many
Unix-like systems, including Linux and FreeBSD. If you get
`Operation not supported` error when using `setfacl` command, please try these
possible solutions before asking.

 . Ext2, Ext3, Ext4 filesystem on Linux: `mount -o acl,remount /path/to/mount/point`
 . tmpfs on Linux should work without problems.
 . UFS on FreeBSD: `mount -o acls,update /path/to/mount/point`
 . tmpfs on FreeBSD does not support ACL. If you really want to use a ramdisk,
   please use `mdconfig -a -t malloc -s size` to create a memory disk device
   and format and mount it like a regular UFS filesystem.


// vim: set ft=asciidoc et:
// Please use the following command to regenerate the PDF.
//  1. Convert asciidoc source to docbook source: asciidoc -b docbook
//  2. Convert docbook source to LaTeX or PDF: pandoc -f docbook -t latex --latex-engine=xelatex -V geometry:margin=40pt -N
// The PDF is generated using asciidoc 8.6.9 and pandoc 1.13.2.
