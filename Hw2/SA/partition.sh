# tested on ubuntu-desktop-12.04.5
# assume there are no partition tables on both disks

fdisk /dev/sda
n
<CR>	# default primary
<CR>	# default 1
<CR>	# default 2048
+200GiB	# "+200G" was used on my virtual machine
t
8e	# beacause there's no any other partition
q

parted /dev/sdb
mklabel	# make a partition table
msdos
mkpart
primary
<CR>	# default ext2
0
300GiB	# "300G" was used on my virtual machine
mkpart
extend
300GiB
350GiB
mkpart
logical
300GiB
350GiB
set
5
lvm
quit

mkfs -t msdos /dev/sdb1

pvcreate /dev/sdb1 /dev/sdc5
vgcreate b02902075 /dev/sdb1 /dev/sdc5
lvcreate -L 150G -n b02902075 b02902075
lvcreate -L 100G -n b02902075-2 b02902075
mkfs -t ext4 /dev/b02902075/b02902075
mkfs -t ext4 /dev/b02902075/b02902075-2
