#! /bin/csh -f

if ($#argv != 1) then
	@ n = 10
else
	@ n = $1
endif

# build word frequency table
tr -s ' ' '\n' | tr -d '[:punct:]' | sort | uniq -ic | sort -nr > freq_table

# check if there exists multiple nth-largest-frequency words

# get the nth largest frequency
set tmp = `head -$n freq_table | tail -1`
set arr = ($tmp)
set nth_freq = ${arr[1]}

# check if the following lines have the same frequency
set lines = `wc -l < freq_table`

@ k = $n + 1
set tmp = `head -$k freq_table | tail -1`
set arr = ($tmp)
set freq = ${arr[1]}

while ($n < $lines && $freq == $nth_freq)
	@ n++
	@ k = $n + 1
	set tmp = `head -$k freq_table | tail -1`
	set arr = ($tmp)
	set freq = ${arr[1]}
end

head -$n freq_table
rm -f freq_table
